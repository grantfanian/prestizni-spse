# Prestižní SPŠE

Userscript se nachází v `prestizni.user.js`.

Pro vygenerování alternativních fotek učitelů spusťte python script `fetch_pfps.py`. Jako argument je vyžadován predict endpoint od [AnimeGANv2](https://huggingface.co/spaces/akhaliq/AnimeGANv2). Vygenerované obrázky budou uloženy do složky `output/`.

#!/usr/bin/env python3

from base64 import b64decode, b64encode
from pathlib import Path
from shutil import rmtree
from sys import argv
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm
import re


OUTPUT_PATH = Path("output/")


def generate_anime_image(predict_url: str, data_uri: str):
    res = requests.post(predict_url, json={
        "data": [
            data_uri,
            "version 2 (🔺 robustness,🔻 stylization)"
        ]
    })
    res.raise_for_status()
    data = res.json()
    return data["data"][0]


def fetch_url_as_data_uri(url: str):
    res = requests.get(url)
    return "data:image/png;base64," + b64encode(res.content).decode("ascii")


def save_data_uri_as_file(path: Path, data_uri: str):
    with open(path, "wb") as file_:
        file_.write(b64decode(data_uri[len("data:image/png;base64,"):].encode("ascii")))


def main():
    if len(argv) != 2:
        print("fetch_pfps.py requires 1 argument")
        exit(1)
    predict_url = argv[1]

    if OUTPUT_PATH.exists():
        rmtree(OUTPUT_PATH)
    OUTPUT_PATH.mkdir()


    res = requests.get("https://www.spse.cz/zamestnanci.php")
    soup = BeautifulSoup(res.text, 'html.parser')

    for element in tqdm(soup.select(".col-md-3 .feature-pinner .header a.color") + soup.select(".col-md-20 .feature-pinner .header a.color")):
        href = element.attrs.get("href")
        m = re.search(r"\d+$", href)
        if m is None:
            continue
        id_ = m.group(0)

        img = element.select_one("img")
        if img is None:
            continue
        src = "https://www.spse.cz/" + img.attrs.get("src")

        source_data_uri = fetch_url_as_data_uri(src)
        output_data_uri = generate_anime_image(predict_url, source_data_uri)
        save_data_uri_as_file(OUTPUT_PATH / (id_ + ".png"), output_data_uri)


if __name__ == "__main__":
    main()
